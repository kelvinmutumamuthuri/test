module PremierSaUssd::Api
  class MainAppController < BaseController

    # GET /ussd_apps
    # GET /ussd_apps.json
    def process_request
      render json: PremierSaUssd::Ussd::RequestDispatcher.call(params)
    rescue => error
      app_logger.error("#{error.message}\n#{error.backtrace.join("\n\t")}")
      response = 'An error happened. Please try again or contact support.'

      render json: { shouldClose: true,
                     ussdMenu: response,
                     responseMessage: response,
                     responseExitCode: 200 }
    end

    private

    def app_logger
      @logger ||= AppLogger.new('ussd_api_logger', { class: 'PremierSaUssd::Ussd', from_host: 'false' })
    end
  end
end