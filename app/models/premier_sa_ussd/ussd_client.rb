module PremierSaUssd
  class UssdClient < Client
    # Callbacks
    before_create :set_defaults

    private

    def set_defaults
      self.client_type ||= :premier
    end
  end
end
