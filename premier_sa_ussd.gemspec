$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "premier_sa_ussd/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "premier_sa_ussd"
  s.version     = PremierSaUssd::VERSION
  s.authors     = ["Kelvin Mutuma"]
  s.email       = ["mutuma@finplusgroup.com"]
  s.homepage    = "https://finplusgroup.com'"
  s.summary     = "This is a USSD engine for Premier Credit South Africa"
  s.description = "This is a USSD engine for Premier Credit South Africa used to implement USSD apps."
  s.license     = "Proprietary license"

  s.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.1"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency 'rspec-rails', '~> 3.7.2'
  s.add_development_dependency 'pry-rails'
end
