# PremierSaUssd
Premier SA USSD App

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'premier_sa_ussd'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install premier_sa_ussd
```

## Contributing
Contribution directions go here.
