module PremierSaUssd
  module Ussd
    class RequestDispatcher
      attr_reader :app, :session, :input, :phone_number, :client, :state, :response, :input

      class << self
        def call(params)
          new(params).process_request
        end
      end

      def initialize(params)
        @app = UssdApp.find_by(app_id: Figaro.env.premier_sa_ussd_app)

        if @app
          @session = AppSession.find_or_create_by(session_id: params[:sessionId], ussd_app_id: app.id)
        end

        @phone_number = '+' + params[:msisdn].to_s.remove('+')
        @session.update(node: app.root_node.id) if session.node.nil?

        @input = params[:text].to_s
        @phone_number = '+' + params[:msisdn].to_s.remove('+')
        @client = PremierSaUssd::UssdClient.find_by(phone_number: phone_number)

        if client && session.client_id == nil
          session.update_column(:client_id, client.id)
        end

        @state = params[:state]
        @response = "END Error"
      end

      def process_request
        if state == 'end'
          { responseMessage: '', responseExitCode: 200 }
        else
          if app
            request_processor_klass = "PremierSaUssd::Ussd::#{nodes_hash[session.current_node.name.to_sym]}".constantize

            if request_processor_klass
              response = request_processor_klass.call(self)
            else
              response = 'END Error. Request count not be processed.'
            end
          end

          close_session = response.starts_with?('END')
          response = response.remove('END ').remove('CON ')

          { shouldClose: close_session,
            ussdMenu: response,
            responseMessage: response,
            responseExitCode: 200 }
        end
      rescue => error
        app_logger.error("#{error.message}\n#{error.backtrace}")
      end

      def nodes_hash
        { root_node: 'RegistrationAndAuthentication::FirstRequest',
          authenticate: 'RegistrationAndAuthentication::Authentication',
          app_proceed: 'RegistrationAndAuthentication::AppProceed',
          new_client: 'RegistrationAndAuthentication::NewClient',
          register: 'RegistrationAndAuthentication::Register',
          mother_maiden_name: 'RegistrationAndAuthentication::MotherMaidenName',
          primary_school_name: 'RegistrationAndAuthentication::PrimarySchoolName',
          forgot_mother_maiden_name: 'ForgotPin::ForgotMotherMaidenName',
          forgot_primary_school_name: 'ForgotPin::ForgotPrimarySchoolName',
          forgot_new_pin: 'ForgotPin::ForgotNewPin',
          forgot_pin_confirm: 'ForgotPin::ForgotPinConfirm',
          request_more_info: 'RegistrationAndAuthentication::RequestMoreInfo',
          enter_new_pin: 'RegistrationAndAuthentication::EnterNewPin',
          confirm_pin: 'RegistrationAndAuthentication::ConfirmPin',
          menu: 'Menu::PremierMenu',
          my_account: 'Account::MyAccount',
          change_pin: 'Account::ChangePin',
          change_enter_new_pin: 'Account::ChangeEnterNewPin',
          change_pin_confirm: 'Account::ChangePinConfirm',
          exit_or_menu: 'Menu::ExitOrMenu',
          forgot_pin_and_security: "ForgotPinAndSecurityQuestions::ForgotPinAndSecurity",
          reset_mother_maiden_name: "ForgotPinAndSecurityQuestions::ResetMotherMaidenName",
          reset_primary_school_name: "ForgotPinAndSecurityQuestions::ResetPrimarySchoolName",
          enter_reset_pin: "ForgotPinAndSecurityQuestions::EnterResetPin",
          confirm_reset_pin: "ForgotPinAndSecurityQuestions::ConfirmResetPin",
        }
      end

      private

      def app_logger
        @logger ||= AppLogger.new('ussd_api_logger', { class: 'PremierSaUssd::Ussd', from_host: 'false' })
      end
    end
  end
end
