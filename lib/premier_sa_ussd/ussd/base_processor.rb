module PremierSaUssd
  module Ussd
    class BaseProcessor
      attr_accessor :request, :session, :current_node, :client, :user_input, :phone_number, :app

      CONTACT_SUPPORT = 'Contact support 0200300500 for assistance.'.freeze
      TRY_AGAIN_OR_CONTACT_SUPPORT = 'Please try again or contact support 0200300500.'.freeze
      ERR_BLANK_USAGE = 'Usage cannot be blank.'.freeze
      ERR_INVALID_USAGE = 'Invalid usage selected.'.freeze
      ERR_FETCHING_LOAN_USAGE = 'An error occurred fetching loan usage. Please try again or contact support 0200300500.'.freeze

      class << self
        def call(request)
          new(request).process_request
        end
      end

      def initialize(request)
        @request = request
        @session = request.session
        @current_node = session.current_node
        @phone_number = request.phone_number
        @client = request.client
        @user_input = request.input
      end

      def process_request
        raise 'Not implemented'
      end

      private

      def set_current_node(node_name)
        session.set_current_node(node_name)
      end

      def current_node_prompt
        session.current_node.prompt
      end

      def current_node_menu_text
        session.current_node.menu_text
      end

      ##
      # Receive a response message that will be displayed to the user and below it the user will be
      # prompted whether they want to exit or go back to the main menu.
      #
      # @param [String] response_message
      #   The response message that will me displayed to the user.
      #
      # @return [String]
      #   Return the received response message and below it there will be a message that prompts
      #   user whether they want to exit or go back to the main menu
      #
      # @example
      #   response = 'You are not approved'
      #   exit_with_response(response)
      #   #=>
      #        "You are not approved
      #        1. Exit
      #        0. Menu"
      def exit_with_response(response_message)
        set_current_node(:exit_or_menu)
        "CON #{response_message}\n\n1: Exit\n0: Menu"
      end

      def display_menu
        menu = ''
        session.current_node.children.each_with_index do |node, index|
          menu << "#{index + 1}. #{node.menu_text}\n"
        end
        "\n#{menu}"
      end

      def logbook_display_menu
        menu = ''
        menu_list = session.current_node.children.to_a.tap(&:pop)
        menu_list.each_with_index do |node, index|
          menu << "#{index + 1}. #{node.menu_text}\n"
        end
        "\n#{menu}"
      end

      ##
      # Calculate months difference between dates
      #
      # @param [DateTime] start_date
      #   Start date
      #
      # @param [DateTime] end_date
      #   End date
      #
      # @return [Integer] Months in integer form
      #
      # @example
      #   start_date = '2020-04-04'.to_time
      #   end_date = '2010-03-04'.to_time
      #   age_in_months(start_date, end_date)
      #   => 118
      def months_difference_between_dates(start_date, end_date)
        months = 0

        years = end_date.year - start_date.year
        months_difference = end_date.month - start_date.month

        if months_difference > 0
          months = months_difference.abs
        elsif months_difference < 0
          years -= 1
          months = 12 - months_difference.abs
        end

        (12 * years) + months
      end

      def app_logger
        @logger ||= AppLogger.new('ussd_api_logger', { class: 'PlatinumUgUssd::Ussd', from_host: 'false' })
      end

      private

      def civil_servants_flat_rates
        {
          '6' => 0.1, '12' => 0.07, '18' => 0.055, '24' => 0.0550, '30' => 0.05, '36' => 0.05,
          '42' => 0.045, '48' => 0.0397, '60' => 0.0370, '72' => 0.0370, '84' => 0.0370,
        }
      end

      def civil_servants_product_rates
        {
          '6' => 184.09, '12' => 130.86, '18' => 102.20, '24' => 98.55, '30' => 88.05,
          '36' => 85.50, '42' => 76.39, '48' => 67.36, '60' => 61.13, '72' => 59.1563,
          '84' => 57.5376,
        }
      end

      def civil_servants_refinance_product_rates
        {
          '6' => 184.09, '12' => 130.86, '18' => 102.1999, '24' => 98.5497, '30' => 88.0467,
          '36' => 85.4995, '42' => 76.3891, '48' => 67.3594, '60' => 61.1334, '72' => 59.1563,
          '84' => 57.5376,
        }
      end

      def client_cs_approved?
        approved = PlatinumUgUssd::Mambu::Clients.
          custom_field(client.mambu_id, 'USSDCSApprovedField')

        approved && approved == 'YES'
      end

      ##
      # Check whether the client is a civil servant. This information is retrieved from a custom
      # field in Mambu.
      #
      # @return [Boolean] Returns true if the client is a civil servant. Meaning the client is
      #   employed by the government.
      def client_civil_servant?
        value = PlatinumUgUssd::Mambu::Clients.
          custom_field(client.mambu_id, 'client_category')

        value == 'C-S Client'
      end

      def logbook_client?
        value = PlatinumUgUssd::Mambu::Clients.
          custom_field(client.mambu_id, 'client_category')

        value == 'LBF Client'
      end

      ##
      # Display Civil Servants terms menu. This is the menu the client is prompted to pick the
      # term they want for the loan based on eligibility. The terms are in months.
      #
      # @param [Array] eligible_terms
      #   This is an array of hashes of eligible terms
      #
      # @return [String] Displays the menu in a text format
      #
      # @example
      #   eligible_terms = [{..}..]
      #   terms_menu(eligible_terms)
      #   #=>
      #       "1. 6
      #        2. 18
      #        3. 24
      #        9. 60"
      def terms_menu(eligible_terms)
        menu = []
        eligible_terms.each_with_index do |elem, index|
          menu << "#{index + 1}. #{elem[:term]}\n"
        end
        "\n#{menu.join}"
      end


      ##
      # Get government employee information from PCA system. The information received is inform of
      # a Hash.
      #
      # @return [Hash] Return the employee information inform of a Hash
      def government_employee
        unrefornmated_employee_id = PlatinumUgUssd::Mambu::Clients.
          custom_field(client.mambu_id, 'EN01')
        @employee_id = reformat_employee_id(unrefornmated_employee_id)
        session.vars['employee_id'] = @employee_id
        session.save

        PlatinumUgUssd::Payroll::Employee.validate(@employee_id)
      end

      ##
      # An employee ID should have 15 digits, this method reformat the ID by prefixing the right
      # number of zeros (0s) if the employee id has less than 15 digits.
      #
      # @param [String] employee_id
      #   The employee ID to be reformatted
      #
      # @return [String] Correctly formatted employee ID
      #
      # @example
      #   employee_id = '0000012345' # 10 digits employee id
      #   reformat_employee_id(employee_id)
      #   #=> '000000000012345'
      def reformat_employee_id(employee_id)
        # Returns the employee_id the way it is if the number of characters is more then 15
        return employee_id if employee_id.size > 15
        missing_zeros = 15 - employee_id.to_s.size

        ('0' * missing_zeros) + employee_id.to_s
      end

      ##
      # Check if client does not have pending Civil Servant USSD loans
      #
      # @return [Boolean] Returns true if client does not have pending Civil Servant USSD loans
      def does_not_have_pending_loans?
        product_key = Figaro.env.civil_servant_product_key.to_s
        pending_loans = PlatinumUgUssd::Mambu::Loans.client_product_pending_loans(client.mambu_id,
                                                                                  product_key)
        pending_loans && pending_loans.count.zero?
      end

      ##
      # Check client employer is blacklisted.
      #
      # @param [Hash] pca_employee_info
      #   This is the employee information in Hash format received from the PCA system
      #
      # @return [String] Returns normal, whitelisted or blacklisted
      #
      # @example
      #   pca_employee_info = {
      #     responsecode: 0,
      #     timestamp: "1552409251",
      #     result: {
      #       employeedetails: {
      #       votecode: "511" # Assuming vote 511 is blacklisted
      #       # There are other items below
      #       }
      #     }
      #     # There are other items below
      #   }
      #   # Assuming vote 511 is blacklisted
      #   employer_vote_type(pca_employee_info)
      #   #=> "blacklisted"
      def employer_vote(pca_employee_info)
        employer_vote_code = pca_employee_info['employeedetails']['votecode']
        vote_code = PlatinumUgUssd::EmployerVote.find_by_vote_code(employer_vote_code)
        return nil unless vote_code
        vote_code
      end

      ##
      # Check whether the payroll date is updated. The current month should mach the payroll
      # updated month.
      #
      # @param [Hash] pca_employee_info
      #   This is the employee information in Hash format received from the PCA system
      #
      # @return [Boolean] Returns true is payroll is updated
      #
      # @example
      #   pca_employee_info = {
      #     responsecode: 0,
      #     timestamp: "1552409251",
      #     result: {
      #       employeedetails: {
      #       lastupdated: "2019-2"
      #       # There are other items below
      #       }
      #     }
      #     # There are other items below
      #   }
      #   # Assuming the date today is 14th March
      #   updated_payroll?(pca_employee_info)
      #   #=> true
      def updated_payroll?
        lastupdated = session.vars['employee_info']['employeedetails']['lastupdated']
        current_month_day_1 = Date.today.beginning_of_month
        lastupdated_month_day_1 = (lastupdated + '-01').to_date

        months_difference = months_difference_between_dates(lastupdated_month_day_1,
                                                            current_month_day_1)

        months_difference <= Figaro.env.payroll_updated_months_difference.to_s.to_i
      end

      ##
      # Calculate client age in months given employee_info from PCA system
      #
      # @param [Hash] employee_info
      #   Employee information from PCA system. The information is in Hash format.
      #
      # @return [Integer] Age in months, which is in integer format
      #
      # @example
      #   #Assume current time is 2020-04-04
      #   employee_info = {'employee_info' => {'employeedetails' => 'dateofbirth' => '2010-03-04'}}
      #   client_age(employee_info)
      #   => 118
      def client_age
        date_of_birth_str = session.vars['employee_info'].try(:[], 'employeedetails').try(:[], 'dateofbirth')

        # Return nil if birtdate for the client is blank
        return unless date_of_birth_str

        # Convert dob which was in string form to time
        date_of_birth = date_of_birth_str.to_time
        age_in_months(date_of_birth)
      end

      ##
      # Calculate age in months
      #
      # @param [DateTime] date_of_birth
      #   Date of birth in Date, DateTime or Time format
      #
      # @return [Integer] Months in integer form
      #
      # @example
      #   # if current time is 2020-04-04
      #   date_of_birth = '2010-03-04'.to_time
      #   age_in_months(date_of_birth)
      #   => 118
      def age_in_months(date_of_birth)
        time_now = Time.now
        months = 0

        years = time_now.year - date_of_birth.year
        months_difference = time_now.month - date_of_birth.month

        if months_difference > 0
          months = months_difference.abs
        elsif months_difference < 0
          years -= 1
          months = 12 - months_difference.abs
        end

        (12 * years) + months
      end

      def loan_terms
        [
          {
            term: 84, max_age: { years: 48, months: 0 }, min_loan_amount: 1_000_000,
            min_take_home_topup: 700_000,
          },
          {
            term: 72, max_age: { years: 49, months: 0 }, min_loan_amount: 500_000,
            min_take_home_topup: 500_000,
          },
          {
            term: 60, max_age: { years: 51, months: 0 }, min_loan_amount: 500_000,
            min_take_home_topup: 400_000,
          },
          {
            term: 48, max_age: { years: 53, months: 0 }, min_loan_amount: 500_000,
            min_take_home_topup: 400_000,
          },
          {
            term: 42, max_age: { years: 53, months: 6 }, min_loan_amount: 500_000,
            min_take_home_topup: 400_000,
          },
          {
            term: 36, max_age: { years: 55, months: 0 }, min_loan_amount: 500_000,
            min_take_home_topup: 400_000,
          },
          {
            term: 30, max_age: { years: 55, months: 6 }, min_loan_amount: 500_000,
            min_take_home_topup: 400_000,
          },
          {
            term: 24, max_age: { years: 57, months: 0 }, min_loan_amount: 200_000,
            min_take_home_topup: 200_000,
          },
          {
            term: 18, max_age: { years: 57, months: 6 }, min_loan_amount: 200_000,
            min_take_home_topup: 200_000,
          },
          {
            term: 12, max_age: { years: 58, months: 0 }, min_loan_amount: 200_000,
            min_take_home_topup: 200_000,
          },
          {
            term: 6, max_age: { years: 58, months: 6 }, min_loan_amount: 200_000,
            min_take_home_topup: 200_000,
          },
        ]
      end

      ##
      # Get the selected term.
      #
      # @param [Integer, String] user_input
      #   The input entered will determine the selected term
      #
      # @return [Integer] Term in months. If invalid input was entered, nil will be returned
      #
      # @example
      #   user_input = 7
      #   loan_term(user_input)
      #   #=> 24
      #
      # @example
      #   user_input = 743343 # Entered invalid term
      #   loan_term(user_input)
      #   #=> nil
      def loan_term(user_input)
        input = user_input.to_i - 1

        if input >= 0
          term = session.vars['eligible_terms'][input]['term']
        end

        term
      rescue
        nil
      end

      ##
      # Get the minimum eligible amount based on the selected term.
      #
      # @param [Integer, String] user_input
      #   The input entered will determine the entered term and in return output the min_loan_amount
      #
      # @return [Float] Minimum eligible amount. If invalid input was entered, nil will be returned
      #
      # @example
      #   user_input = 7
      #   minimum_eligible_amount(user_input)
      #   #=> 40000
      #
      # @example
      #   user_input = 743343 # Entered invalid term
      #   minimum_eligible_amount(user_input)
      #   #=> nil
      def minimum_eligible_amount(user_input)
        input = user_input.to_i - 1

        if input >= 0
          term = session.vars['eligible_terms'][input]['min_loan_amount']
        end

        term
      end

      def minimum_eligible_topup_amount(user_input)
        input = user_input.to_i - 1

        if input >= 0
          term = session.vars['eligible_terms'][input]['min_take_home_topup']
        end

        term
      end

      ##
      # Compute affordability amount based on the selected term
      #
      # @param [Integer, String] term
      #   This this is the term in months the user selected. It can be 6, 12, 24, e.t.c.
      #
      # @return [Float] Affordability amount
      #
      # @example
      #   term = 72
      #   affordability(term)
      #   #=> 7364837.98
      def affordability(term, existing_deduction_amount: 0)
        term = term.to_i
        rate = civil_servants_flat_rates[term.to_s]

        maximum_affordability = session.
            vars['employee_info']['employeedetails']['maximum_affordability'] +
            existing_deduction_amount

        if session.vars['hardship_allowance']
          alternate_affordability = session.
            vars['employee_info']['employeedetails']['alt_affordability']
          hardship_allowance = session.
            vars['employee_info']['employeedetails']['non_recurring_allowances'].
              find { |allowance| allowance['description'] == 'Hardship Allowance' }
          other_non_recurring_allowances = session.
            vars['employee_info']['employeedetails']['non_recurring_allowances'].
              select { |allowance| allowance['description'] != 'Hardship Allowance' }
          other_non_recurring_amount = other_non_recurring_allowances.
              sum { |allowance| allowance['amount'].to_f }
          if hardship_allowance && alternate_affordability
            hardship_allowance_amount = 0.48 * hardship_allowance['amount'].to_f
            if maximum_affordability > 0 || alternate_affordability - (0.48 * other_non_recurring_amount) >= hardship_allowance_amount
              maximum_affordability += hardship_allowance_amount
            elsif alternate_affordability - (0.48 * other_non_recurring_amount) < hardship_allowance_amount
              maximum_affordability = maximum_affordability + alternate_affordability - (0.48 * other_non_recurring_amount)
            end
          end
        end

        if session.vars['short_term_reservation']
          maximum_affordability += session.vars['short_term_reservation']['installmentamount'].to_f
        end

        affordability_amount = ((maximum_affordability - 6000) * term) / ((rate * term) + 1)

        if affordability_amount > 15_000_000
          15_000_000.00
        else
          affordability_amount.floor(-2) - 100
        end
      end

      def session_logger(action, error)
        session.logs << {
          action: action,
          reason: error,
          time: Time.now.strftime('%d/%m/%Y %H:%M'),
        }
        session.save
      end

      def new_client_session_logger(ipps, action, error)
        session.vars['new_ipps_number'] = ipps
        session.logs << {
            action: action,
            reason: error,
            time: Time.now.strftime('%d/%m/%Y %H:%M'),
        }
        session.phone_number = phone_number
        session.save
      end

      def client_is_active?(mambu_client)
        %w(ACTIVE INACTIVE).include?(mambu_client['state'])
      end

      def no_pending_loans?
        pending_loans_filter = [
          ['ACCOUNT_STATE', 'IN', %w(APPROVED PENDING_APPROVAL PARTIAL_APPLICATION)],
          ['ACCOUNT_HOLDER_KEY', 'EQUALS', client.mambu_id],
        ]

        pending_loans = FinplusSuitePlatinumUssd::Mambu::Loans.
          search(pending_loans_filter, 1)

        pending_loans && pending_loans.count.zero?
      end

      def active_short_term_loans
        pending_loans_filter = [
            ['ACCOUNT_STATE', 'IN', %w(ACTIVE ACTIVE_IN_ARREARS)],
            ['ACCOUNT_HOLDER_KEY', 'EQUALS', client.mambu_id],
            ['TOTAL_BALANCE', 'MORE_THAN', 0],
            ['NUM_INSTALLMENTS', 'EQUALS', 1],
        ]

        short_term_loans = FinplusSuitePlatinumUssd::Mambu::Loans.
            search(pending_loans_filter, 1)

        short_term_loans
      end

      def no_written_off_loans?
        pending_loans_filter = [
            ['ACCOUNT_STATE', 'EQUALS', 'CLOSED_WRITTEN_OFF'],
            ['ACCOUNT_HOLDER_KEY', 'EQUALS', client.mambu_id],
        ]

        written_off_loans = PlatinumUgUssd::Mambu::Loans.
            search(pending_loans_filter, 1)

        written_off_loans && written_off_loans.count.zero?
      end

      def written_off_loans
        pending_loans_filter = [
            ['ACCOUNT_STATE', 'EQUALS', 'CLOSED_WRITTEN_OFF'],
            ['ACCOUNT_HOLDER_KEY', 'EQUALS', client.mambu_id],
        ]

        written_off_loans = PlatinumUgUssd::Mambu::Loans.
            search(pending_loans_filter, 1, sort_params: %w(CLOSED_DATE DESCENDING))

        written_off_loans
      end

      def send_compliance_message(message)
        message_to_send = "Dear #{client.first_name} To complete the processing of your "\
          "application, please #{message}. For queries call "\
          "0200300500. There when you need us."
        PlatinumUgUssd::Infobip.
          send_message(client.phone_number.remove('+'), message_to_send)
      end

      def node_data
        session.current_node.data
      end

      def loan_usage_selection
        loan_usage_sel = PlatinumUgUssd::Mambu::CustomFields.fetch('LU03')

        return unless loan_usage_sel

        loan_usage_sel['customFieldSelectionOptions']
      end

      def loan_usage_menu(options)
        menu = []
        options.each_with_index do |elem, index|
          menu << "#{index + 1}. #{elem['value']}\n"
        end
        "\n#{menu.join}"
      end

      def loan_usage(user_input)
        input = user_input.to_i - 1

        if input >= 0
          term = session.vars['loan_usage_selections'][input]
        end

        term
      rescue
        nil
      end

      def calculate_top_up_balance(loan, loan_repayments, product, lgf)
        grace_period = 0.0
        prepaid_interest = 0.0
        prepaid_fees = 0.0
        unpaid_fees = 0.0
        unpaid_penalties = 0.0
        refinance_accrued_interest_fee = 0.0
        lgf_balance = lgf['availableBalance'].to_f
        default_fee = loan_repayments[product['minNumInstallments'].to_i - 1]['feesDue']
        current_period = loan_repayments.find { |repayment| repayment['dueDate'].to_date > Date.today }
        current_period_due_date = current_period ? current_period['dueDate'].to_date : Date.today
        last_repayment = loan_repayments.last
        first_repayment = loan_repayments.first
        last_period_due_date = last_repayment['dueDate'].to_date
        first_period_due_date = first_repayment['dueDate'].to_date
        differing_recalculation = false
        reference_date = loan['disbursementDetails']['disbursementDate'].to_date
        interest_balance = loan['interestBalance'].to_f
        refinance_period = loan_repayments.count
        if (first_period_due_date - reference_date).to_i > 44
          number_of_installments = (Date.today - reference_date).to_f/30
          date_since_dis = Date.today + (first_period_due_date - reference_date).to_i.days - 30.days
          current_period = loan_repayments.find { |repayment| repayment['dueDate'].to_date >  date_since_dis }
          current_period_due_date = current_period ? current_period['dueDate'].to_date : Date.today
          differing_recalculation = true
          loan_repayments.each_with_index do |repayment, i|
            current_period_number = i + 1
            if current_period_number > number_of_installments.ceil && repayment['state'] != 'PENDING'
              prepaid_interest += repayment['interestPaid'].to_f
              prepaid_fees += repayment['feesPaid'].to_f
            end
            if current_period_number < number_of_installments.ceil
              unpaid_fees += repayment['feesDue'].to_f - repayment['feesPaid'].to_f
              unpaid_penalties += repayment['penaltyDue'].to_f - repayment['penaltyPaid'].to_f
              refinance_accrued_interest_fee += repayment['interestDue'].to_f - repayment['interestPaid'].to_f
            end
            if current_period_number == number_of_installments.ceil
              unpaid_fees += repayment['feesDue'].to_f - repayment['feesPaid'].to_f
              unpaid_penalties += repayment['penaltyDue'].to_f - repayment['penaltyPaid'].to_f
              if number_of_installments.modulo(1) < 0.5
                refinance_accrued_interest_fee += repayment['interestDue'].to_f / 2  - repayment['interestPaid'].to_f
              else
                refinance_accrued_interest_fee += repayment['interestDue'].to_f  - repayment['interestPaid'].to_f
              end
            end
            if i > product['maxNumInstallments'].to_i - 1
              refinance_period -= 1
              grace_period_paid = repayment['feesDue'].to_f - default_fee.to_f - repayment['feesPaid'].to_f
              if grace_period_paid > 0
                grace_period += grace_period_paid
                if repayment['dueDate'].to_date <= current_period_due_date && repayment['state'] != 'PAID'
                  unpaid_fees -= grace_period_paid
                end
              end
            end
          end
          refinance_accrued_interest_fee -= interest_balance
        end
        unless differing_recalculation
          unpaid_fees += loan['feesDue'].to_f
          unpaid_penalties += loan['penaltyDue'].to_f
          loan_repayments.each_with_index do |repayment, i|
            if repayment['dueDate'].to_date > Date.today && repayment['state'] != 'PENDING'
              prepaid_interest += repayment['interestPaid'].to_f
              prepaid_fees += repayment['feesPaid'].to_f
            end
            if repayment['dueDate'].to_date == current_period_due_date && repayment['state'] != 'PAID' && last_period_due_date != Date.today
              unpaid_fees += repayment['feesDue'].to_f
              unpaid_penalties += repayment['penaltyDue'].to_f - repayment['penaltyPaid'].to_f
            end
            if i > product['maxNumInstallments'].to_i - 1
              refinance_period -= 1
              grace_period_paid = repayment['feesDue'].to_f - default_fee.to_f - repayment['feesPaid'].to_f
              if grace_period_paid > 0
                grace_period += grace_period_paid
                if repayment['dueDate'].to_date <= current_period_due_date && repayment['state'] != 'PAID'
                  unpaid_fees -= grace_period_paid
                end
              end
            end
          end
          payment = loan_repayments.find { |repayment| repayment['dueDate'].to_date.beginning_of_month == Date.today.beginning_of_month }
          next_payment = loan_repayments.find { |repayment| repayment['dueDate'].to_date.beginning_of_month == Date.today.next_month.beginning_of_month }
          if loan['lastInterestAppliedDate']
            reference_date = loan['lastInterestAppliedDate'].to_date
          end
          if Date.today < first_period_due_date
            refinance_accrued_interest_fee += loan['accruedInterest'].to_f
          else
            if payment
              if payment['dueDate'].to_date <= Date.today
                if (Date.today - reference_date).to_i < 15
                  refinance_accrued_interest_fee += next_payment['interestDue'].to_f / 2 if next_payment
                else
                  refinance_accrued_interest_fee += next_payment['interestDue'].to_f if next_payment
                end
              else
                if (Date.today - reference_date).to_i < 15
                  refinance_accrued_interest_fee += payment['interestDue'].to_f / 2 if payment
                else
                  refinance_accrued_interest_fee += payment['interestDue'].to_f if payment
                end
              end
            end
          end
        end
        if prepaid_interest > 0
          session.logs << {
              action: 'Civil Servant Refinance',
              reason: "Prepaid Interest",
              time: Time.now.strftime('%d/%m/%Y %H:%M'),
          }
        end
        top_up_balance = loan['principalBalance'].to_f.round(2) - prepaid_interest.round(2) -
            prepaid_fees.round(2) + unpaid_fees.round(2) + unpaid_penalties.round(2) +
            refinance_accrued_interest_fee.round(2) + interest_balance.round(2) +
            grace_period.round(2) - lgf_balance.round(2)
        if session.vars['short_term_loan']
          top_up_balance = top_up_balance + session.vars['short_term_loan']['principalBalance'].to_f +
              session.vars['short_term_loan']['feesBalance'].to_f + session.vars['short_term_loan']['interestBalance'].to_f +
              session.vars['short_term_loan']['penaltyBalance'].to_f
        end
        total_unpaid_fees = unpaid_fees.round(2) + grace_period.round(2)
        top_up_and_lgf_bal = top_up_balance.round(2) + lgf_balance.round(2)
        session.vars['refinance_values'] = {
          refinance_principal_balance: loan['principalBalance'].to_f.round(2),
          interest_balance: interest_balance.round(2) + grace_period.round(2),
          grace_period_fees: grace_period.round(2),
          prepaid_interest: prepaid_interest.round(2),
          prepaid_fees: prepaid_fees.round(2),
          unpaid_fees: unpaid_fees.round(2),
          unpaid_penalties: unpaid_penalties.round(2),
          refinance_accrued_interest_fee: refinance_accrued_interest_fee.round(2),
          top_up_balance: top_up_balance.round(2),
          current_period: months_difference_between_dates(loan['disbursementDetails']['disbursementDate'].to_date, Date.today),
          refinance_term: loan_repayments.count,
          refinance_period: refinance_period,
          refinance_admin_fee: default_fee.to_f.round(2),
          lgf_balance: lgf_balance.round(2),
          total_unpaid_fees: total_unpaid_fees.round(2),
          top_up_and_lgf_bal: top_up_and_lgf_bal.round(2),
        }
        session.save
        top_up_balance.round(2)
      end

      def refinance_affordability(term)
        term = term.to_i
        rate = civil_servants_flat_rates[term.to_s]

        maximum_affordability = session.
          vars['employee_info']['employeedetails']['maximum_affordability']

        if session.vars['hardship_allowance']
          alternate_affordability = session.
            vars['employee_info']['employeedetails']['alt_affordability']
          hardship_allowance = session.
            vars['employee_info']['employeedetails']['non_recurring_allowances'].
              find { |allowance| allowance['description'] == 'Hardship Allowance' }
          other_non_recurring_allowances = session.
            vars['employee_info']['employeedetails']['non_recurring_allowances'].
              select { |allowance| allowance['description'] != 'Hardship Allowance' }
          other_non_recurring_amount = other_non_recurring_allowances.
              sum { |allowance| allowance['amount'].to_f }
          if hardship_allowance && alternate_affordability
            hardship_allowance_amount = 0.48 * hardship_allowance['amount'].to_f
            if maximum_affordability > 0 || alternate_affordability >= hardship_allowance_amount
              maximum_affordability += hardship_allowance_amount
            elsif alternate_affordability < hardship_allowance_amount
              maximum_affordability = maximum_affordability + alternate_affordability - (0.48 * other_non_recurring_amount)
            end
          end
        end

        if session.vars['short_term_reservation']
          maximum_affordability += session.vars['short_term_reservation']['installmentamount'].to_f
        end

        total_affordability = maximum_affordability + session.vars['current_deduction'].to_f

        affordability_amount = ((total_affordability - 6000) * term) / ((rate * term) + 1)
        refinance_affordability_amount = affordability_amount

        if refinance_affordability_amount > 15_000_000
          15_000_000.00
        else
          refinance_affordability_amount.floor
        end
      end

      ##
      # Get client's PCA running deduction if it's there
      def client_running_deduction
        client_deductions = session.vars['employee_info']['employeedetails']['deductions'].
            select do |deduction|
          deduction['deductiontype'] == Figaro.env.payroll_company_code
        end

        if client_deductions.count == 1
          client_deductions.first if client_deductions.first['status'] == 'takenup'
        end
      end

      ##
      # Check whether if a client has pending reservations. This will return true only if
      # client does not have any pending reservations/deductions or has a taken up reservation.
      def has_no_pending_reservations?
        pending_deductions = session.vars['employee_info']['employeedetails']['deductions'].select do |deduction|
          deduction['deductiontype'] == Figaro.env.payroll_company_code &&
              deduction['status'] != 'takenup'  && !deduction['referencecode'].ends_with?('STL')
        end

        pending_deductions && pending_deductions.count.zero?
      end

      ##
      # Create loan lead
      def create_loan_lead
        @loan_lead = PlatinumUgUssd::LoanLead.new(
            phone_number: phone_number,
            client_names: session.vars['loans_lead_full_name'],
            product_name: session.vars['loans_lead_product_name'],
            session_data: session.vars['lead_session_data'],
            status: :pending
        )

        if @loan_lead.save
          return "END Thank you for your inquiry. One of our agents will get back to you shortly."
        end

        app_logger.error("LoanLead could not be created. PhoneNumber: #{phone_number}, "\
        "SessionID: #{session.id}, Error: #{@loan_lead.errors.full_messages.to_sentence}")
        "END Error submitting information. Please try again or contact support at 0709900000"
      rescue => ex
        app_logger.error("LoanLead could not be created. PhoneNumber: #{phone_number}, "\
        "SessionID: #{session.id}, Error: #{ex.message}\n#{ex.backtrace.join("\n\t")}")
        "END Error submitting information. Please try again or contact support at 0709900000"
      end

      ##
      # Menu for existent phone numbers
      def display_menu_for_existent_phone_numbers
        menu = ''
        menu_texts = session.current_node.children.pluck(:menu_text)
        menu_texts.delete_at(-1)
        menu_texts.each_with_index do |menu_text, index|
          menu << "#{index + 1}. #{menu_text}\n"
        end
        "\n#{menu}"
      end

      def has_no_taken_up_reservations?
        takenup_deductions = session.vars['employee_info']['employeedetails']['deductions'].select do |deduction|
          deduction['deductiontype'] == Figaro.env.payroll_company_code &&
              deduction['status'] == 'takenup'
        end

        takenup_deductions && takenup_deductions.count.zero?
      end

      def has_taken_up_reservations?
        takenup_deductions = session.vars['employee_info']['employeedetails']['deductions'].select do |deduction|
          deduction['deductiontype'] == Figaro.env.payroll_company_code &&
              deduction['status'] == 'takenup'
        end

        takenup_deductions && takenup_deductions.count.one?
      end

      def short_term_reservations
        pending_deductions = session.vars['employee_info']['employeedetails']['deductions'].select do |deduction|
          deduction['deductiontype'] == Figaro.env.payroll_company_code &&
              deduction['status'] != 'takenup'  && deduction['referencecode'].ends_with?('STL')
        end

        pending_deductions
      end

      def no_active_short_term_loans?
        pending_loans_filter = [
            ['ACCOUNT_STATE', 'IN', %w(ACTIVE ACTIVE_IN_ARREARS)],
            ['ACCOUNT_HOLDER_KEY', 'EQUALS', client.mambu_id],
            ['TOTAL_BALANCE', 'MORE_THAN', 0],
            ['NUM_INSTALLMENTS', 'EQUALS', 1],
        ]

        short_term_loans = FinplusSuitePlatinumUssd::Mambu::Loans.
            search(pending_loans_filter, 1)

        short_term_loans && short_term_loans.count.zero?
      end
    end
  end
end
