module PremierSaUssd::Ussd
  module ForgotPinAndSecurityQuestions
    class EnterResetPin < BaseProcessor
      def process_request
        if AppLib.valid_pin?(user_input)
          session.vars[:reset_pin] = user_input

          set_current_node(:confirm_reset_pin)
          "CON #{current_node_prompt}"
        else
          set_current_node(:enter_reset_pin)
          "CON PIN should be a four digit number. #{current_node_prompt}"
        end
      end
    end
  end
end
