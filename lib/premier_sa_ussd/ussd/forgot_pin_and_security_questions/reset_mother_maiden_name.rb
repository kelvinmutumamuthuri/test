module PremierSaUssd::Ussd
  module ForgotPinAndSecurityQuestions
    class ResetMotherMaidenName < BaseProcessor
      def process_request
        if user_input.blank?
          'END This question cannot be blank.'
        else
          session.vars[:reset_mother_maiden_name] = user_input.downcase

          set_current_node(:reset_primary_school_name)
          "CON #{current_node_prompt}"
        end
      end
    end
  end
end
