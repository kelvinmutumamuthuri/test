module PremierSaUssd::Ussd
  module ForgotPinAndSecurityQuestions
    class ResetPrimarySchoolName < BaseProcessor
      def process_request
        if user_input.blank?
          'END This question cannot be blank.'
        else
          session.vars[:reset_primary_school_name] = user_input.downcase

          set_current_node(:enter_reset_pin)
          "CON #{current_node_prompt}"
        end
      end
    end
  end
end
