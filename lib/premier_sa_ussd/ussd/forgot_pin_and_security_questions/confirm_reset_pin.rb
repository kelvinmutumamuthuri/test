module PremierSaUssd::Ussd
  module ForgotPinAndSecurityQuestions
    class ConfirmResetPin < BaseProcessor
      def process_request
        if AppLib.valid_pin?(user_input)
          if session.vars['reset_pin'] == user_input

            client.pin_number = AppLib.hash_ids_encode(user_input.split(''))
            client.reset_pin = false
            client.otp = nil
            client.otp_expiry = nil
            client.question1 = session.vars['reset_mother_maiden_name']
            client.question2 = session.vars['reset_primary_school_name']

            client.logs << {
              action: 'PIN Reset',
              name: "#{client.first_name} #{client.last_name}",
              user_enc_key: client.mambu_id,
              description: 'Client has reset their PIN which was initiated by a staff.',
              type: :client,
              time: Time.now.strftime('%d/%m/%Y %H:%M'),
            }

            if client.save
              'END PIN has successfully been reset.'
            else
              'END An error happened. Please try again or contact support on +27 (0) 87 131 0060'
            end
          else
            'END PIN did not match.'
          end
        else
          'END PIN should be a four digit number.'
        end
      end
    end
  end
end
