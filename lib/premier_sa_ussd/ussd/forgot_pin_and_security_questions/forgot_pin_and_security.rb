module PremierSaUssd::Ussd
  module ForgotPinAndSecurityQuestions
    class ForgotPinAndSecurity < BaseProcessor
      def process_request
        if client.otp
          decoded_otp = AppLib.hash_ids_decode(client.otp).join('')
          if decoded_otp == user_input
            if DateTime.now < client.otp_expiry
              set_current_node(:reset_mother_maiden_name)
              "CON #{current_node_prompt}"
            else
              'END The OTP is already expired.'
            end
          else
            'END You have entered the wrong OTP. Please try again.'
          end
        else
          'END No OTP has been generated. Call +27 (0) 87 131 0060 to receive one.'
        end
      end
    end
  end
end
