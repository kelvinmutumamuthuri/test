module PremierSaUssd::Ussd
  module RegistrationAndAuthentication
    class ConfirmPin < BaseProcessor

      def process_request
        pin = user_input
        if AppLib.valid_pin?(pin) # Check if PIN is 4 digits
          if session.vars['new_pin'] == pin
            client = PremierSaUssd::UssdClient.new(first_name: session.vars['first_name'],
                                                    last_name: session.vars['last_name'],
                                                    phone_number: request.phone_number,
                                                    id_number: session.vars['mambu_id'],
                                                    mambu_id: session.vars['mambu_enc_key'],
                                                    question1: session.vars['mother_maiden_name'],
                                                    question2: session.vars['primary_school_name'],
                                                    pin_number: AppLib.hash_ids_encode(pin),
                                                    logs: [{ action: 'Registered', name: "#{session.vars['first_name']} #{session.vars['last_name']}",
                                                             user_enc_key: session.vars['mambu_id'], description: 'Registered and accepted T&C.',
                                                             type: :client, time: Time.now.strftime('%d/%m/%Y %H:%M') }])

            if client.save
              session.update_column(:client_id, client.id)
              set_current_node(:premier_menu)
              "CON Please select an option below #{display_menu}"
            else
              app_logger.error("Client could not be saved: #{client.errors.full_messages.to_sentence}")
              "END An error happened. Please try again or contact support on +27 (0) 87 131 0060."
            end
          else
            "END PIN did not match. Please try again."
          end
        else
          "END PIN should be a four digit number."
        end
      end
    end
  end
end
