module PremierSaUssd::Ussd
  module RegistrationAndAuthentication
    class AppProceed < BaseProcessor
      ERR_BLACKLISTED = "END You are not allowed to access this service. "\
      "Please call +27 (0) 87 131 0060 for assistance."

      ERR_FETCHING = "END An error occurred fetching your data. "\
      "Please call +27 (0) 87 131 0060 for assistance."

      def process_request
        client_data = PremierSaUssd::Mambu::Clients.fetch(client.mambu_id)

        return ERR_FETCHING unless client_data
        return ERR_BLACKLISTED unless client_is_active?(client_data)
        decoded_pin = AppLib.hash_ids_decode(client.pin_number).join

        if decoded_pin == user_input
          set_current_node(:premier_menu)
          "Please select an option below #{display_menu}"
        else
          "END You have entered the wrong PIN. Please try again."
        end
      end
    end
  end
end