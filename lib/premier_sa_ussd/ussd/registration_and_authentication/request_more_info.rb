module PremierSaUssd::Ussd
  module RegistrationAndAuthentication
    class RequestMoreInfo < BaseProcessor

      def process_request
        set_current_node(:request_more_info)
        "END #{current_node_prompt}"
      end
    end
  end
end
