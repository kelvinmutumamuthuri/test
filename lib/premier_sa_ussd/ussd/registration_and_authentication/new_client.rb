module PremierSaUssd::Ussd
  module RegistrationAndAuthentication
    class NewClient < BaseProcessor

      def process_request
        # Process request differently for phone numbers that do not exist in Mambu
        if session.vars['phone_number_does_not_exists_in_mambu'] == true
          return process_request_for_non_existent_phone_numbers
        end

        if user_input == '1' # Register
          set_current_node(:register)

          "CON #{current_node_prompt}"
        elsif user_input == '2'
          set_current_node(:request_more_info)

          "END #{current_node_prompt}"
        else
          "END Invalid input"
        end
      end

      private

      ##
      # Process request differently for phone numbers that do not exist in Mambu
      def process_request_for_non_existent_phone_numbers
        if user_input == '1'
          return "END You already have a pending request. You'll be contacted once it's "\
            "processed"
          set_current_node(:loans_leads)
          "CON #{current_node_prompt}"
        elsif user_input == '2'
          set_current_node(:request_more_info)

          "END #{current_node_prompt}"
        else
          "END Invalid input"
        end
      end
    end
  end
end
