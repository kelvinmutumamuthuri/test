module PremierSaUssd::Ussd
  module RegistrationAndAuthentication
    class MotherMaidenName < BaseProcessor

      def process_request
        if user_input.blank?
          'END This question cannot be blank.'
        else
          session.vars[:mother_maiden_name] = user_input.downcase

          set_current_node(:primary_school_name)
          "CON #{current_node_prompt}"
        end
      end
    end
  end
end
