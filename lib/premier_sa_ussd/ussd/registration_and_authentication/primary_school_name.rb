module PremierSaUssd::Ussd
  module RegistrationAndAuthentication
    class PrimarySchoolName < BaseProcessor

      def process_request
        if user_input.blank?
          'END This question cannot be blank.'
        else
          session.vars[:primary_school_name] = user_input.downcase

          set_current_node(:enter_new_pin)
          "CON #{current_node_prompt}"
        end
      end
    end
  end
end
