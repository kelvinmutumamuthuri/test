module PremierSaUssd::Ussd
  module RegistrationAndAuthentication
    class EnterNewPin < BaseProcessor

      def process_request
        pin = user_input
        if AppLib.valid_pin?(pin)
          session.vars[:new_pin] = pin
          session.save

          set_current_node(:confirm_pin)
          "CON #{current_node_prompt}"
        else
          "END PIN should be a four digit number."
        end
      end
    end
  end
end
