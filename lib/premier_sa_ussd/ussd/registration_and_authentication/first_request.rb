module PremierSaUssd::Ussd
  module RegistrationAndAuthentication
    class FirstRequest < BaseProcessor
      def process_request
        if client
          set_current_node(:authenticate)

          response = "CON Welcome back #{client.first_name}. Please select from "
          response << "the options below #{display_menu}"
        else
          if phone_number_exists_in_mambu?
            set_current_node(:new_client)
            "CON #{current_node_menu_text}".
              gsub('#Menu#', display_menu_for_existent_phone_numbers)
          else
            session.vars['phone_number_does_not_exists_in_mambu'] = true
            set_current_node(:new_client)
            "CON Welcome to Premier Credit..  By proceeding you agree that you have read "\
            "and accepted the Ts and Cs at https://bit.ly/3dyBFyN #Menu#".
              gsub('#Menu#', display_menu_for_non_existent_phone_numbers)
          end
        end
      end

      private
      
      ##
      # Check if phone number exists in Mambu
      def phone_number_exists_in_mambu?
        clients = PremierSaUssd::Mambu::Clients.
          fetch_by_phone_number(phone_number.gsub('+', ''))

        return true if clients && clients.count > 0
      end

      ##
      # Menu for non existent phone numbers
      def display_menu_for_non_existent_phone_numbers
        menu = ''
        menu_texts = session.current_node.children.pluck(:menu_text)[-2..-1].reverse
        menu_texts.each_with_index do |menu_text, index|
          menu << "#{index + 1}. #{menu_text}\n"
        end
        "\n#{menu}"
      end
    end
  end
end
