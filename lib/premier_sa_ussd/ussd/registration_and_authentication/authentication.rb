module PremierSaUssd::Ussd
  module RegistrationAndAuthentication
    class Authentication < BaseProcessor

      def process_request
        if user_input == '1'
          set_current_node(:app_proceed)
          "CON #{current_node_prompt}"
        elsif user_input == '2'
          set_current_node(:forgot_mother_maiden_name)
          "CON #{current_node_prompt}"
        elsif user_input == '3'
          set_current_node(:forgot_pin_and_security)
          "CON #{current_node_prompt}"
        else
          "END Invalid input"
        end
      end
    end
  end
end