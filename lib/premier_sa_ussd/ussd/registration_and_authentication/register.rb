module PremierSaUssd::Ussd
  module RegistrationAndAuthentication
    class Register < BaseProcessor

      def process_request
        # Check if client exists in mambu
        # Search using ID

        mobile_number = '+' + request.phone_number.to_s.remove('+')
        client_data = PremierSaUssd::Mambu::Clients.fetch_by_phone_number(mobile_number)

        # client_fields = FinplusSuitePlatinumUssd::Mambu::Clients.fetch(user_input, true)

        if client_data
          if client_is_active?(client_data)
            session.vars[:first_name] = client_data['firstName']
            session.vars[:last_name] = client_data['lastName']
            session.vars[:mambu_id] = client_data['id']
            session.vars[:mambu_enc_key] = client_data['encodedKey']

            session.save
            set_current_node(:mother_maiden_name)
            "CON #{current_node_prompt}"
          else
            "END You are not allowed to access this service. Please call +27 (0) 87 131 0060 for assistance."
          end
        else
          "END Your registration details cannot be confirmed. Please call +27 (0) 87 131 0060 for assistance."
        end
      end
    end
  end
end
