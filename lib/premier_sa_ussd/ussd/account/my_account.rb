module PremierSaUssd::Ussd
  module Account
    class MyAccount < BaseProcessor

      def process_request
        if user_input == '1'
          set_current_node(:change_pin)
          "CON #{current_node_prompt}"
        else
          set_current_node(:exit_or_menu)
          response = "CON Invalid input"
          response << "\n0.Back Menu\n1.Exit"
        end
      end
    end
  end
end
