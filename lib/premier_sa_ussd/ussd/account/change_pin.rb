module PremierSaUssd::Ussd
  module Account
    class ChangePin < BaseProcessor
      def process_request
        current_pin = AppLib.hash_ids_decode(client.pin_number).join.to_s

        if current_pin == user_input
          set_current_node(:change_enter_new_pin)
          "CON #{current_node_prompt}"
        else
          set_current_node(:exit_or_menu)
          response = "CON You have entered the wrong PIN. Please try again."
          response << "\n0.Back Menu\n1.Exit"
        end
      end
    end
  end
end
