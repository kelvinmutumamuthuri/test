module PremierSaUssd::Ussd
  module Account
    class ChangePinConfirm < BaseProcessor
      def process_request
        if AppLib.valid_pin?(user_input)
          if session.vars['new_pin'] == user_input
            client.pin_number = AppLib.hash_ids_encode(user_input)

            if client.save
              set_current_node(:exit_or_menu)
              response = "CON Your PIN has been successfully changed."
              response << "\n0.Back Menu\n1.Exit"
            else
              set_current_node(:exit_or_menu)
              logger.error("Client could not be saved: #{client.errors.full_messages.to_sentence}")
              response = "CON An error happened. Please try again or contact support on 0200300500."
              response << "\n0.Back Menu\n1.Exit"
            end
          else
            set_current_node(:exit_or_menu)
            response = "CON PIN did not match."
            response << "\n0.Back Menu\n1.Exit"
          end
        else
          set_current_node(:exit_or_menu)
          response = "CON PIN should be a four digit number."
          response << "\n0.Back Menu\n1.Exit"
        end
      end
    end
  end
end
