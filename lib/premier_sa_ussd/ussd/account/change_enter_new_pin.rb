module PremierSaUssd::Ussd
  module Account
    class ChangeEnterNewPin < BaseProcessor

      def process_request
        if AppLib.valid_pin?(user_input)
          session.vars['new_pin'] = user_input
          session.save

          set_current_node(:change_pin_confirm)
          "CON #{current_node_prompt}"
        else
          set_current_node(:exit_or_menu)
          response = "CON PIN should be a four digit number."
          response << "\n0.Back Menu\n1.Exit"
        end
      end
    end
  end
end
