module PremierSaUssd::Ussd
  module Menu
    class ExitOrMenu < BaseProcessor
      def process_request
        if user_input == '1'
          'END You have ended the session'
        elsif user_input == '0'
          set_current_node(:menu)
          "CON Please select from the options below #{display_menu}"
        else
          'END Invalid input'
        end
      end
    end
  end
end