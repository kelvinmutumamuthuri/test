##
# PremierSaUssd is the engine namespace and Ussd is the module with everything about
# USSD application
module PremierSaUssd::Ussd
  ##
  # This module contains classes about USSD app main menu management. This is available to the user
  # when they login into the app.
  module Menu
    class PremierMenu < BaseProcessor
      ##
      # This is the method that processes the request in this class
      def process_request
        if user_input == '1'
          set_current_node(:loan_application)
          response = "CON " + display_menu
          response << "\n00. Back"
        elsif user_input == '2'
          set_current_node(:check_loan_balance)
          response = "CON " + display_menu
          response << "\n00. Back"
        elsif user_input == '3'
          set_current_node(:my_account)
          "CON #{current_node_menu_text} #{display_menu}"
        else
          "END Invalid input"
        end
      end
    end
  end
end
