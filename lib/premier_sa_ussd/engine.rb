module PremierSaUssd
  class Engine < ::Rails::Engine
    isolate_namespace PremierSaUssd

    config.generators do |g|
      g.test_framework :rspec
    end

    # Include the Engine's migrations into the parent apps migrations
    initializer :append_migrations do |app|
      unless app.root.to_s.match root.to_s
        app.config.paths['db/migrate'].concat(config.paths['db/migrate'].expanded)
      end
    end
  end
end
