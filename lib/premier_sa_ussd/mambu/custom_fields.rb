module PremierSaUssd::Mambu
  class CustomFields < MambuBase
    class << self
      def fetch(custom_field_id, full_details = false)
        params = full_details ? { fullDetails: true } : {}
        call.fetch_object_by_id('customfield', custom_field_id, params: params)
      end
    end
  end
end