module PremierSaUssd::Mambu
  class Clients < MambuBase
    class << self
      def fetch(client_id)
        call.fetch_object_by_id('client', client_id)
      end

      def fetch_by_phone_number(phone_number, limit: 1)
        filters_cons = {
          filterConstraints: [{
                                filterSelection: 'MOBILE_PHONE_NUMBER',
                                filterElement: "EQUALS",
                                value: phone_number,
                              }],
        }

        clients = call.base_search_objects('client', filters_cons, 1)
        clients.first if clients
      end

      def fetch_by_national_id(client_id)
        clients = call.fetch_object_by_id('client', "?idDocument=#{client_id}")

        clients.first if clients && clients.count == 1
      end

      def fetch_full_details(client_id)
        call.fetch_object_by_id('client', "#{client_id}?fullDetails=true")
      end

      def fetch_by_a_custom_field(custom_field_value, custom_field_id)
        filters_cons = { "filterConstraints": [{ "dataFieldType": "CUSTOM",
                                                 "filterSelection": custom_field_id,
                                                 "filterElement": "EQUALS",
                                                 "value": custom_field_value.to_s }] }

        clients = call.base_search_objects('client', filters_cons, 1)
        clients.first if clients
      end

      def fetch_by_a_custom_field_and_phone_number(custom_field_value, custom_field_id,
                                                   phone_number)
        filters_cons = { "filterConstraints": [{ "dataFieldType": "CUSTOM",
                                                 "filterSelection": custom_field_id,
                                                 "filterElement": "EQUALS",
                                                 "value": custom_field_value.to_s },
                                               { "filterSelection": 'MOBILE_PHONE_NUMBER',
                                                 "filterElement": "EQUALS",
                                                 "value": phone_number }] }

        clients = call.base_search_objects('client', filters_cons, 1)
        clients.first if clients
      end

      def saving_accounts(client_id)
        data = call.fetch_object_by_id("clients/#{client_id}/saving", nil)

        data ? data : []
      end

      def custom_field(client_id, field_id)
        fields = call.fetch_object_by_id('client', "#{client_id}/custominformation/#{field_id}")

        field = fields.first if fields

        field['value'] if field
      end

      def update_fields(client_id, fields)
        call.update_custom_fields(client_id, 'client', fields)
      end

      def delete_field(client_id, field_id)
        call.delete_object_by_id('client', "#{client_id}/custominformation/#{field_id}")
      end

      def client_branch(client_id)
        client = fetch_by_national_id(client_id)
        Branches.fetch(client['assignedBranchKey']) if client
      end

      def client_lgfs(client_enc_key)
        filters_cons = [['PRODUCT_KEY', 'EQUALS', Figaro.env.lgf_product_key],
                        ['ACCOUNT_HOLDER_KEY', 'EQUALS', client_enc_key],
                        ['ACCOUNT_STATE', 'IN', %w(APPROVED ACTIVE)]]

        data = call.search_objects('saving', filters_cons)

        if data
          data
        else
          []
        end
      end

      def cs_clients_search(limit, offset: 0)
        filters_cons = { "filterConstraints": [{ "dataFieldType": "CUSTOM",
                                                 "filterSelection": 'client_category',
                                                 "filterElement": "EQUALS",
                                                 "value": 'C-S Client' }] }
        call.base_search_objects('client', filters_cons, limit, offset: offset)
      end
    end
  end
end
