module PremierSaUssd
  module Mambu
    class MambuBase
      attr_accessor :basic_auth, :base_url, :options

      def self.call
        new
      end

      def initialize
        @basic_auth = { username: Figaro.env.mambu_api_user, password: Figaro.env.mambu_api_password }
        @base_url = Figaro.env.mambu_domain
        @options = { headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' },
                     basic_auth: @basic_auth }
      end

      def search_objects(object, filters, limit = 50, sort_params = nil, offset: 0)
        filters_cons = { filterConstraints: [] }

        # set sortDetails if sort_params is passed
        filters_cons[:sortDetails] = { sortingColumn: sort_params[0], sortingOrder: sort_params[1] } if sort_params

        filters.each do |filter|
          if filter.count == 2
            filters_cons[:filterConstraints] << { filterSelection: filter[0], filterElement: filter[1] }
          elsif filter.count == 4
            filters_cons[:filterConstraints] << { filterSelection: filter[0], filterElement:
              filter[1], value: filter[2], dataFieldType: filter[3] }
          else
            if filter[2].class.to_s == 'Array'
              filters_cons[:filterConstraints] << { filterSelection: filter[0], filterElement: filter[1], values: filter[2] }
            else
              filters_cons[:filterConstraints] << { filterSelection: filter[0], filterElement: filter[1], value: filter[2] }
            end
          end
        end
        base_search_objects(object, filters_cons, limit, offset: offset)
      end

      def base_search_objects(object, filters_cons, limit, offset: 0)
        url = Addressable::URI.encode(base_url + "/api/#{object}s/search?offset=#{offset}&limit=#{limit}")

        options[:body] = filters_cons.to_json

        # Log request
        app_logger.info("Request. Search #{object.camelcase}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}")

        response = HTTParty.post(url, options) # Make request

        # Log response
        app_logger.info("Response. Search #{object.camelcase}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                          "Response Code: #{response.code}, Response: #{response.body}")

        if response.code == 200
          data = JSON.parse(response.body)
        else
          app_logger.error("Error. Search #{object.camelcase}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                             "Response Code: #{response.code}, Response: #{response.body}")
        end

        data
      end

      def fetch_object_by_id(object, id, limit: 50, full_details: false, params: {})
        id = id.to_s.strip
        params[:fullDetails] = true if full_details
        url_params = CGI.unescape(AppLib.parameterize(params)) + "&offset=0&limit=#{limit}"
        api_object = object == 'search' ? object : "#{object}s"
        url = Addressable::URI.encode(@base_url + "/api/#{api_object}/#{id}?#{url_params}")
        @options.delete(:headers)

        # Log request
        app_logger.info("Request. Fetch #{object.camelcase}, Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}")

        response = HTTParty.get(url, @options) # Make request

        app_logger.info("Response. Fetch #{object.camelcase}, Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}, " +
                          "Response Code: #{response.code}, Response: #{response.body}")

        if response.code == 200
          data = JSON.parse(response.body)
        else
          app_logger.error("Error. Fetch #{object.camelcase}, Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}, " +
                             "Response Code: #{response.code}, Response: #{response.body}")
        end

        data
      end

      def delete_object_by_id(object, id)
        id = id.to_s.strip
        url = Addressable::URI.encode(base_url + "/api/#{object}s/#{id}")
        options.delete(:headers)

        # Log request
        app_logger.info("Request. Delete #{object.camelcase} with ID: #{id}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}")

        response = HTTParty.delete(url, options) # Make request

        app_logger.info("Response. Delete #{object.camelcase} with ID: #{id}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                          "Response Code: #{response.code}, Response: #{response.body}")

        if response.code == 200
          data = JSON.parse(response.body)
        else
          app_logger.error("Error. Delete #{object.camelcase} with ID: #{id}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                             "Response Code: #{response.code}, Response: #{response.body}")
        end

        data
      end

      def transaction_action(account_id, object, type, request_options = {})
        if type == :approve
          body = { type: 'APPROVAL' }
        elsif type == :disburse
          body = {
            type: 'DISBURSEMENT', method: request_options[:method],
            bookingDate: Time.now.strftime('%F'),
            valueDate: Time.now.strftime('%F')
          }
        elsif type == :withdraw
          body = { type: 'WITHDRAWAL', amount: request_options[:amount], method: request_options[:method] }
        elsif type == :fees
          body = { type: 'FEE', repayment: request_options[:repayment], notes: request_options[:notes] }
        elsif type == :approval_request
          body = { type: 'PENDING_APPROVAL' }
        elsif type == :loan_withdraw
          body = { type: 'WITHDRAW' }
        else
          body = request_options
        end

        if request_options[:frd]
          body[:firstRepaymentDate] = request_options[:frd]
        end

        if request_options[:maker_checker] == true
          options[:basic_auth] = { username: Figaro.env.mambu_api_disburser_user,
                                   password: Figaro.env.mambu_api_disburser_password }
        end

        custom_fields = request_options[:custom_fields]

        # Add custom fields id and value to transaction
        if custom_fields
          body[:customInformation] = []
          custom_fields.each do |field|
            body[:customInformation] << { customFieldID: field[:field_name],
                                          value: field[:field_value] }
          end
        end

        # Add fees encoded keys to transaction
        fees = request_options[:fees]
        if fees
          body[:fees] = fees
        end
        url = base_url + "/api/#{object}s/#{account_id}/transactions/"
        options[:body] = body.to_json

        # Log request
        app_logger.info("Request. Transaction Action for #{account_id}, Endpoint: #{url}, Request:#{filter_sensitive_info(options)}")

        response = HTTParty.post(url, options)

        app_logger.info("Response. Transaction Action for #{account_id}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                          "Response Code: #{response.code}, Response: #{response.body}")

        if response.code == 200 || response.code == 201
          data = JSON.parse(response.body)
        else
          app_logger.error("Error. Transaction Action for #{account_id}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                             "Response Code: #{response.code}, Response: #{response.body}")
        end
        data
      end

      def create_mambu_loan(client_key, loan_amount, params)
        url = base_url + '/api/loans/'
        options[:body] = { loanAccount: { accountHolderKey: client_key,
                                          accountHolderType: 'CLIENT',
                                          productTypeKey: params[:product_key],
                                          interestRate: params[:rate],
                                          repaymentPeriodUnit: params[:repayment_period_unit],
                                          repaymentInstallments: params[:installments],
                                          loanAmount: loan_amount }, customInformation: [] }

        custom_fields = params[:custom_fields]
        if custom_fields
          custom_fields.each do |field|
            options[:body][:customInformation] << { customFieldID: field[:field_name],
                                                    value: field[:field_value] }
          end
        end

        options[:body] = options[:body].to_json

        # Log request
        app_logger.info("Request. Create Loan for Client #{client_key}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}")

        response = HTTParty.post(url, options)

        app_logger.info("Response. Create Loan for Client #{client_key}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                          "Response Code: #{response.code}, Response: #{response.body}")

        if response.code == 201
          data = JSON.parse(response.body)
        else
          app_logger.error("Error. Create Loan for Client #{client_key}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                             "Response Code: #{response.code}, Response: #{response.body}")
        end

        data
      end

      def create_journal_entry(request_options)
        url = base_url + '/api/gljournalentries/'
        options[:body] = request_options.to_json

        if request_options[:maker_checker] == true
          options[:basic_auth] = { username: Figaro.env.mambu_api_disburser_user,
                                   password: Figaro.env.mambu_api_disburser_password }
        end

        # Log request
        app_logger.info("Request. Create Journal Entry. Endpoint: #{url}, Request: #{filter_sensitive_info(options)}")

        response = HTTParty.post(url, options)

        # Log response
        app_logger.info("Response. Create Journal Entry. Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                          "Response Code: #{response.code}, Response: #{response.body}")

        if response.code != 201
          app_logger.error("Error. Create Journal Entry. Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                             "Response Code: #{response.code}, Response: #{response.body}")
        end

        response
      end

      def update_custom_fields(object_id, object, custom_fields)
        url = base_url + "/api/#{object}s/#{object_id}/custominformation"
        options[:body] = { customInformation: custom_fields }.to_json

        # Log request
        app_logger.info("Request. Updating #{object.camelcase} custom fields #{object_id}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}")

        response = HTTParty.patch(url, options) # Make request

        # Log response
        app_logger.info("Response. Updating #{object.camelcase} custom fields #{object_id}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                          "Response Code: #{response.code}, Response: #{response.body}")

        if response.code == 200
          data = JSON.parse(response.body)
        else
          app_logger.error("Error. Updating #{object.camelcase} custom fields #{object_id}, Endpoint: #{url}, Request: #{filter_sensitive_info(options)}, " +
                             "Response Code: #{response.code}, Response: #{response.body}")
        end

        data
      end

      def refinance_civil_servant(loan, options)
        loan_id = loan['id']

        body = {
          action: 'refinance',
          loanAccount: {
            productTypeKey: options[:product_type_key],
            loanAmount: options[:loan_amount],
            interestRate: options[:interest_rate],
            repaymentInstallments: options[:repayment_installments],
            repaymentPeriodUnit: options[:repayment_period_unit],
            disbursementDetails: {
              firstRepaymentDate: (Date.today + options[:frd_offset].days).to_s
            }
          },
          customInformation: [
            { value: options[:top_up_amount], customFieldID: 'rf01' },
          ],
          restructureDetails: {
            topUpAmount: options[:top_up_amount],
            feesWriteOff: options[:fees_write_off],
          },
        }

        url = @base_url + "/api/loans/#{loan_id}/action"
        @options[:body] = body.to_json

        app_logger.info("Request. Refinancing a loan.Acc. Key: #{loan_id}, "\
        "Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}")

        response = HTTParty.post(url, @options)

        app_logger.info("Response. Refinancing a loan. Acc. Key: #{loan_id}, "\
                             "Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}, Code:#{response.code}, Response: #{response.body.inspect}")
        if response.code == 200
          data = { data: JSON.parse(response.body) }
        else
          app_logger.error("Error. Refinancing a loan. Acc. Key: #{loan_id}, "\
                             "Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}, Code: #{response.code}, Response: #{response.body.inspect}")
          data = { reason: JSON.parse(response.body).map { |k, v| "#{k}: #{v}" }.join(', ') }
        end
        data
      end

      def create_lbf_refinance(loan, options)
        custom_fields = []

        loan['customFieldValues'].each do |field|
          none_transferred_field_sets_array = [
            'Top Up Application',
            'Sales Representative',
            'Buy Off Details',
            'Refinance Details',
            'USSD Refinance Details',
            'Settlement Details',
            'Relationship Officers',
            'Pre-disbursement Calls',
            'Disbursement Details',
            'Collections Team Feedback',
            'Travel Details',
            'LBF & PSL Appraisal Details',
            'LBF PORTFOLIO MANAGEMENT',
            'Branch Compliance',
            'Credit Compliance',
            'LBF RO Questionnaire',
            'Refinance Details',
            'Call Center USSD',
            'Appraiser1',
            'Appraiser 2'
          ]
          if !(none_transferred_field_sets_array.include?(field['customField']['customFieldSet']['name']) ||
            %w(rf01 USSD_LBF_Prequalified LT01 s_no EN2 EC01 client_advance client_advance_status VT01 pe01 Prepaid_Account_At_Refinance original_account_id LU03).include?(field['customFieldID'])) &&
            field['customField']['state'] == 'NORMAL'
            if field['value']
              custom_fields << { value: field['value'], customFieldID: field['customFieldID'] }
            elsif field['linkedEntityKeyValue']
              custom_fields << { linkedEntityKeyValue: field['linkedEntityKeyValue'], customFieldID: field['customFieldID'] }
            end
          end
        end

        body = {
          loanAccount: {
            accountHolderKey: options[:client_key],
            accountHolderType: 'CLIENT',
            productTypeKey: options[:product_type_key],
            loanAmount: options[:loan_amount],
            interestRate: options[:interest_rate],
            repaymentPeriodCount: options[:repayment_period_count],
            repaymentInstallments: options[:repayment_installments],
            repaymentPeriodUnit: options[:repayment_period_unit],
            gracePeriodType: options[:grace_period_type],
          },
          customInformation: custom_fields + [
            { value: options[:top_up_amount].to_i.to_s, customFieldID: 'rf01' },
            { value: options[:top_up_balance], customFieldID: 'TB02' },
            { value: options[:usage], customFieldID: 'LU03' },
            { value: options[:original_account_id], customFieldID: 'original_account_id' }
          ],
        }

        if options[:prepaid_interest]
          body[:customInformation].append({ value: 'YES', customFieldID: 'Prepaid_Account_At_Refinance' })
        end

        url = @base_url + "/api/loans"
        @options[:body] = body.to_json

        app_logger.info("Request. Create USSD LBF Refinance loan, "\
          "Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}")

        response = HTTParty.post(url, @options)

        app_logger.info("Response. Creating USSD LBF Refinance loan. Client Key: "\
          "#{options[:client_key]}, Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}, "\
          "Code:#{response.code}, Response: #{response.body}")

        if response.code == 201
          data = JSON.parse(response.body)
        else
          app_logger.error("Error. Creating USSD LBF Refinance loan. Client Key: "\
          "#{options[:client_key]}, Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}, "\
          "Code:#{response.code}, Response: #{response.body}")
        end

        data
      end

      def add_comment(loan_id, text)
        body = { comment: { text: text } }
        url = base_url + +"/api/loans/#{loan_id}/comments"
        options[:body] = body.to_json
        # Log request
        app_logger.info("Request. Add comment for a Loan #{loan_id}, Endpoint: #{url}, Request:#{filter_sensitive_info(options)}")

        response = HTTParty.post(url, options) # Make request

        # Log response
        app_logger.info("Response. Add comment for a Loan #{loan_id}, Endpoint: #{url}, Request:#{filter_sensitive_info(options)}, " +
                          "Response Code: #{response.code}, Response: #{response.body}")

        if response.code == 201
          data = JSON.parse(response.body)
        else
          app_logger.error("Error. Add comment for a Loan #{loan_id}, Endpoint: #{url}, Request:#{filter_sensitive_info(options)}, " +
                             "Response Code: #{response.code}, Response: #{response.body}")
        end

        data
      end

      def create_cs_refinance(loan, options)
        custom_fields = []

        loan['customFieldValues'].each do |field|
          if %w(s_no oc ws wsc BN01 BB02 SC02).include?(field['customFieldID']) &&
            field['customField']['state'] == 'NORMAL'
            if field['value']
              custom_fields << { value: field['value'], customFieldID: field['customFieldID'] }
            elsif field['linkedEntityKeyValue']
              custom_fields << {
                linkedEntityKeyValue: field['linkedEntityKeyValue'],
                customFieldID: field['customFieldID']
              }
            end
          end
        end

        body = {
          loanAccount: {
            accountHolderKey: options[:client_key],
            accountHolderType: 'CLIENT',
            productTypeKey: options[:product_key],
            loanAmount: options[:loan_amount],
            interestRate: options[:rate],
            repaymentPeriodCount: options[:repayment_period_count],
            repaymentInstallments: options[:installments],
            repaymentPeriodUnit: options[:repayment_period_unit],
            gracePeriodType: options[:grace_period_type],
          },
          customInformation: custom_fields + [
            { value: options[:top_up_amount].to_i.to_s, customFieldID: 'rf01' },
            { value: options[:top_up_balance], customFieldID: 'TB02' },
            { value: options[:original_account_id], customFieldID: 'original_account_id' },
            { value: options[:custom_fields].first[:field_value], customFieldID: options[:custom_fields].first[:field_name] }
          ],
        }

        url = @base_url + "/api/loans"
        @options[:body] = body.to_json

        app_logger.info("Request. Create USSD CS Refinance loan, "\
          "Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}")

        response = HTTParty.post(url, @options)

        app_logger.info("Response. Creating USSD CS Refinance loan. Client Key: "\
          "#{options[:client_key]}, Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}, "\
          "Code:#{response.code}, Response: #{response.body}")

        if response.code == 201
          data = JSON.parse(response.body)
        else
          app_logger.error("Error. Creating USSD CS Refinance loan. Client Key: "\
          "#{options[:client_key]}, Endpoint: #{url}, Request: #{filter_sensitive_info(@options)}, "\
          "Code:#{response.code}, Response: #{response.body}")
        end

        data
      end

      def filter_sensitive_info(params)
        # Used Marshal to avoid changing the original params object
        Marshal.load(Marshal.dump(params)).tap do |new_params|
          if new_params[:basic_auth]
            # Filter Mambu username and password
            new_params[:basic_auth][:username] = '[FILTERED]' if new_params[:basic_auth][:username]
            new_params[:basic_auth][:password] = '[FILTERED]' if new_params[:basic_auth][:password]
          end
        end
      end

      def app_logger
        @logger ||= AppLogger.new('mambu_api', { class: 'MambuAPI', from_host: 'false' })
      end
    end
  end
end