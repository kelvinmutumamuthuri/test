namespace :premier_sa_ussd do
  desc "Regenerate ussd tree"
  task :import_ussd_tree => :environment do
    file_path = PremierSaUssd::Engine.root.join('ussd_tree.json')
    ussd_app = UssdApp.first_or_create(name: 'Premier SA USSD App', app_id: 'premier_sa_ussd_application')

    if File.exists? file_path
      json_tree = JSON.parse(File.read(file_path))
      ussd_app.app_nodes.delete_all

      generate_ussd_tree(json_tree, ussd_app)
    end
  end

  private

  def generate_ussd_tree(json, ussd_app, parent_id = nil)
    app_node = ussd_app.app_nodes.new json.except('children', 'id')
    app_node.ussd_app_id = ussd_app.id
    app_node.parent_id = parent_id
    if json['name'] && json['node_type']
      app_node.save!
    else
      app_node.save!(validate: false)
    end

    json['children'].each do |child_json|
      generate_ussd_tree(child_json, ussd_app, app_node.id)
    end
  end
end