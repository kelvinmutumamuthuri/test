require 'premier_sa_ussd/engine'
require 'premier_sa_ussd/mambu/mambu_base'
require 'premier_sa_ussd/mambu/clients'
require 'premier_sa_ussd/mambu/custom_fields'

Gem.find_files(PremierSaUssd::Engine.root.
  join('lib/premier_sa_ussd/ussd/*.rb')).each { |path| require path }
Gem.find_files(PremierSaUssd::Engine.root.
  join('lib/premier_sa_ussd/ussd/*/*.rb')).each { |path| require path }
Gem.find_files(PremierSaUssd::Engine.root.
  join('lib/premier_sa_ussd/ussd/*/*/*.rb')).each { |path| require path }
if Rails.env != 'test'
  Gem.find_files(PremierSaUssd::Engine.root.
    join('lib/premier_sa_ussd/mambu_apps/*.rb')).each { |path| require path }
end


module PremierSaUssd
end
