PremierSaUssd::Engine.routes.draw do
  post 'api/main_app/process_request/:session/:sessionId/:state', to: 'api/main_app#process_request'
  put 'api/main_app/process_request/:session/:sessionId/:state', to: 'api/main_app#process_request'
end
